const axios = require('axios');
require('dotenv').config();

const apiHost = 'http://localhost:4000';

async function addUser(userData) {
  try {
    const {data} = await axios.post(
      apiHost + '/users/signin',
      userData,
      {
        headers:
        {
          'Content-Type': 'application/json'
        }
      }
    );
    return(data);
  } catch(err) {
    console.log('Error-->', err);
  }
}

async function loginUser(userData) {
  try {
    const {data} = await axios.post(
      apiHost + 'users/login',
      userData,
      {
        headers:
        {
          'Content-Type': 'application/json'
        }
      }
    );
    return(data);
  } catch(err) {
    console.log('Error-->', err);
  }
}

async function getUser(id) {
}

function deleteUser(id) {

}

module.exports = {
  addUser, loginUser, getUser, deleteUser
};
