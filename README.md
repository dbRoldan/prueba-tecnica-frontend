# Projecto realizado con VueJS
Este proyecto abarca la creacion y logueo del usuario con los elementos de seguridad debidamente creados respecto al manejo de cookies; no obstante la visualizacion del perfil, asi como la modificacion y eliminacion del mismo no se siguieron partiendo de los requerimientos expuestos a partir del enunciado del problema. Para seguir el proceso de ejecucion del mismo se debe tener en cuentaque esta e comunica con un API local, asi para su ejecucion a traves de la red simplemente se requerira la direccion del api en el *.env* con su puerto en caso de ser requerido, tener en cuenta que i este no es insertado, se tendra por defecto el puerto 8080:

```
APIHOST=http://direccion.api:puerto
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
